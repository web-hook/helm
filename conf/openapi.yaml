openapi: 3.0.0
info:
  title: Webhooks
  summary: Interactive documentation for webhooks.app
  description: |
    Interactive documentation to use the webhooks.app tool

    Because it can be sometimes useful to have access to a webhook from anywhere,
    this simple application can be used for tests and demo in a few seconds.

    The flow is straightforward:
    - create a webhook and get a new authentication token
    - reference that webhook (https://webhooks.app + auth header) in a tool that uses that alerting medium
    - check the content received by the webhook
    - remove the webhook and all its related data once you are done

    webhooks.app does not connect any applications, it just means to be used to vizualise the payloads sent by various application interacting using WebHooks.
  contact:
    name: API Support
    email: luc@techwhale.io
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
servers:
  - description: Main environment
    url: https://webhooks.app
  - description: Demo environment
    url: https://webhooks.techwhale.io
paths:
  "/wh":
    get:
      tags:
        - Webhooks management
      summary: Define a new webhook and get a unique authentication token
      operationId: "createWebhook"
      responses:
        "201":
          description: webhook created
    delete:
      tags:
        - Webhooks management
      summary: Delete a webhook and all its related content
      operationId: "deleteWebhook"
      responses:
        "200":
          description: webhook deleted
        "401":
          description: Invalid token
      security:
        - bearerAuth: []
  "/wh/info":
    get:
      tags:
        - Webhooks management
      summary: Get info about the current webhook
      operationId: "createWebhookInfo"
      responses:
        "200":
          description: information returned
      security:
        - bearerAuth: []
  "/data":
    post:
      tags:
        - Data handling
      summary: Send json payload
      operationId: "sendDataToWebhook"
      requestBody:
        description: Whatever json payload
        required: true
        content:
          application/json:
            schema:
              type: object
      responses:
        "200":
          description: payload received
        "401":
          description: Invalid token
      security:
        - bearerAuth: []
    get:
      tags:
        - Data handling
      summary: Get the timestamped list of payloads received by the webhook
      operationId: "getDataFromWebhook"
      responses:
        "200":
          description: list of payload received
        "401":
          description: Invalid token
      security:
        - bearerAuth: []
  "/stats":
    get:
      tags:
        - Simple statistics
      summary: Get simple counters
      operationId: "getStatistics"
      responses:
        "200":
          description: returns the number of existing webhooks and payloads

components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer

tags:
  - name: Webhooks management
  - name: Data handling
  - name: Simple statistics
